# Mid-senior mobile engineer challenge

For this challenge you will need to authenticate to our auth server and execute a few requests on our challenges api.
We expect a mobile solution built on swift (iOS), Kotlin (Android) or Flutter.
We estimate that this challenge will need between **15h-25h** to be completed.
We will evaluate your project organization, coding style and completeness.

If you are having problems understanding the challenge or implementing any part of it, feel free to contact us.

When you have a solution that you feel confortable with, create a PR for this repo with your solution under `yyyymm > your_username` (eg. 202010 > bacp).


## Objective

The mobile app should allow a user to authenticate using our OAuth server, after that step is completed, the application should present a screen with a string requested to the API and have a call to action that allows the user to execute the encryption of the string and send it back to the api.

## Mobile app

* Your app must be able to login a user and execute a request on our [API endpoints](https://w4hss5fjh7.execute-api.eu-central-1.amazonaws.com/Prod/swagger/index.html) to perform the actions described. 
* The app package name should be `com.kenbi.mobile` 
* After login you must show a screen where we can click a button that:
  * Calls your GET endpoint to retrieve the string from your API.
  * Create an SHA256 hash of that string.
  * POST it to our API.

## Authentication

* OAuth server `https://squirtle.challenges.kenbi.systems/`.
* You need to implement three authentication screens to be able to login against our OAuth server
	* On stage #1 the user needs to input his phone number.
	* On stage #2 the user confirms his data.
	* On stage #3 the user inputs the code received by SMS and app sends the OAuth the code to exchange it for an access token to be used to access the API above.


### Stage #1

###### Method `POST`

###### Endpoint`/auth/sms/candidate`

######Headers `Content-Type:application/json`

###### Body
	{
	    "phoneNumber":"+351xxxxxx"
	}

##### Response OK
###### Headers `Content-Type:	application/json`
###### HTTP Status `200`

###### Body
	{
	    "phoneNumber": "+351xxxxxx",
	    "ownerName": "B**** P****",
	    "roleValidationToken": "CfDJ8GenWiRADhJEvdI01y0TAMTsmz2fSJjo9"
	}

##### Response NOK
###### Headers `Content-Type:	application/json`
###### HTTP Status `404`

###### Body
	{
	    "type": "USER_NOT_FOUND",
	    "title": "Not Found",
	    "status": 404,
	    "traceId": "|562ae3fa-4a1be145fe9a3658."
	}



### Stage #2

###### Method `POST`

###### Endpoint`/auth/sms/send`

######Headers `Content-Type:application/json`

###### Body
	{
	    "phoneNumber":"+351xxxxxx",
	    "roleValidationToken": "CfDJ8GenWiRADhJEvdI01y0TAMTsmz2fSJjo9"
	}

##### Response OK
###### Headers `Content-Type:	application/json`
###### HTTP Status `200`

###### Body
	{
	    "resend_token": "CfDJ8GenWiRADhJEvdI01y0TAMRoGqoX5OYOD"
	}

##### Response NOK
###### Headers `Content-Type:	application/json`
###### HTTP Status `404`

###### Body
	{
	    "type": "INVALID_TOKEN",
	    "title": "Bad Request",
	    "status": 400,
	    "traceId": "|562ae400-4a1be145fe9a3658."
	}
	


### Stage #3

Exchange the code received by SMS by the access token, and to do so, you need you should implement OpenID Connect.
You should use the token obtained to make requests to our [API](https://w4hss5fjh7.execute-api.eu-central-1.amazonaws.com/Prod/swagger/index.html)

Please refer to the [Authorization service configuration](https://github.com/openid/AppAuth-Android#authorization-service-configuration) section on AppAuth for andoid.
As guide, please see the example bellow in how to implement it with our OAuth server.

	// AUTHORIZE_ENDPOINT   				-> can be obtainged on the "OpenID Connect discovery document" of our OAuth api
	// TOKEN_ENDPOINT						-> can be obtainged on the "OpenID Connect discovery document" of our OAuth api
	// GRANT_TYPE_PHONE  					-> "phone_number_token"
	// scopes            					-> "openid","profile","testAPI"
	// phoneNumber							-> user phone number
	// accessCode       				 	-> the code sent by SMS
	
	// Create the authorization code grant request
	val tokenRequest = TokenRequest.Builder(
	    AuthorizationServiceConfiguration(
	        Uri.parse(AUTHORIZE_ENDPOINT),
	        Uri.parse(TOKEN_ENDPOINT)
	    ),
	    CLIENT_ID
	)
	    .setGrantType(GRANT_TYPE_PHONE)
	    .setScopes(scopes)
	    .setAdditionalParameters(
	        mapOf(
	            "phone_number" to phoneNumber,
	            "verification_token" to accessCode
	        )
	    )
	    .build()
		
	var appAuthConfigBuilder = AppAuthConfiguration.Builder()
	appAuthConfigBuilder.setConnectionBuilder(ConnectionBuilderForTesting())
	
	// Trigger the request
	val authService =
	    AuthorizationService(context, appAuthConfigBuilder.build())
	authService.performTokenRequest(
	    tokenRequest,
	    NoClientAuthentication.INSTANCE,
	    callback
	)


### If you have any doubts or questions, please feel free to reach out to us.


 
