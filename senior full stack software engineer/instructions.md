# Senior developer challenge

For this challenge you will need to authenticate to our OAuth2 server, create an API and a client application and, optionaly deploy the solution on the cloud.

Both are expected to be created using .Net Core 3.1.
We estimate that this challenge will need 30-35h to be completed.
We will evaluate your project organization, coding style and completeness.

If you are having problems understanding the challenge or implementing any part of it, feel free to contact us.

When you have a solution that you feel confortable with, create a PR for this repo with your solution under `yyyymm > your_username` (eg. 202010 > bacp).

## Objective

Implement a solution that provides both a frontend and backend.

The frontend should allow the user to authenticate in our OAuth2 server and present a screen where the user can execute the encryption of the returned data and send it back to the api, which then should store the encrypted data on the database.

## Authentication

* Our server for you to get your auth token is https://squirtle.challenges.kenbi.systems/.
* Our server is restricting authentication only from clients working on `http://localhost:5001`.

## Database

* Create a database named `yyyymm_<your_username>` (eg. 202010_bacp)
* Create a table named `Challenge_<your_username>` on the provided DBMS with all the collumns you see fit to solve this problem in the best way possible. 
* We will share with you the DBMS information that you need to accomplish the task above.

## API

* Your API must be configured to be protected by our OAuth2 server. For that purpose we will provide you privately the secret that you must use in your API configuration to identify as `testAPI`.
* You must implement a GET action for any given controller that returns the string "**alive**". This action must be unsecured.
* You must implement a GET action for any given controller that returns the string "**unencrypted_value**". This action must be secured.
* You must implement a POST action for any given controller that receives a processed string and saves it to the database via the means you consider necessary. This action must be secured.

## Client App

* Your client app must be able to login a user and call on demand your API endpoints to perform the actions described.
* You need to connect with a standard OpenIDConnect authentication for a `code` response type and using PKCE.
* These are the scopes we expect you to need to use our OAuth2 server: `openid`, `profile`, `offline_access`, `testAPI`.
* After login you must show a web page where we can click a button that:
  * Calls your GET endpoint to retrieve the string from your API.
  * Creates an SHA256 hash of that string.
  * POSTs it to your API.

## Deployment to the cloud

* You will be provided a set of credentials to a cloud provider were you can deploy the solution the way you see fit.
* We will take into consideration the scalability and cost-effective factor of the solution.

